// conditionall statements 

// conditional statements allow usn to  perform taks based on a condition

let num1 = 0;

// if the statements - if statements allows us to perform a task if the condition given is true


/*
    if(condition)
    tasl/code to perform
*/
// rund code if the condition is true
if (num1===0) {
    console.log("the value of num1 is 0");
    
}

num1 = 25;
if (num1===0) {
    console.log("the curent value of num1 is still 0")
}

let city = "New York";

if (city === "new Jersey") {
    console.log("Welcome to New Jersey")
}


// how we can a respones to failed condition?

//else -- excecutes a code/task if thr previsin condition/s are not met


if (city==="new Jersey") {
    console.log("Welcome to New Jersey");
    
}else{
    console.log("this is not to New Jersey");
}


if (num1<20) {
    console.log("num1's value is less than 20 ");
    
}else{
    console.log("num's value more than 20");
}



// can we use if-else in a function ?
// yes. this improves reusability of our code

function cityChesk(city) {
        if (city ==="New York") {
            console.log("Welcome to the Empire State")
            
        }else{
            console.log("you are not in new work");
        }
}

cityChesk("New York")
cityChesk("Los Angeles")


function nuMbuget(total) {

    if (total <= 4000) {
        console.log("My budget is 3000");
        console.log("you are still with in budget");
        
    }else{
        console.log("My budget is 5000");
        console.log("you are currenty over budget");
    }
    
}

nuMbuget(3000);
nuMbuget(5000);


// Can we then, if a condiotion is not met, shw a differennt respones if another spedified condiotn is met instead

// else if
// allow us to execute code/tasks if the previus condtion/s are not met or false and IF the specified condion is met instead


let city2 = "Manila";
if (city2 === "New York"){
    console.log("Welcome to New work!");
}else if (city2 ==="Manila") {
        console.log("Welcome to Manila");
}else{
    console.log("I dont know where you are");
}

//else and else if statements are you optional, meaning in a condition chain/statements, there should alwqay be an if statements.you cam skip oor not add else if or else statements


if (city2==="New York") {
        console.log("Welcome to New work!");    
}
else if(city2==="Manila"){
    console.log("i keep coming back in manila ");    

}else{
    console.log("Where bong");    
}

// usually we only have a single else statements. because else is run when all consdition have not been met

// let role = "admin";

// if (role=== "developer") {
//     console.log("Welcome back, Developer"); 
// }else{
//     console.log("Role Provide is invalid");
// }else{
    
// }

function deterMIneTyphoonIntensity(windSpeed) {
    if (windSpeed < 30) {
        return 'Not a typhoon yet.';
    }else if (windSpeed <= 61) {
        return "Tropical Despression Detected.";
        
    }else if (windSpeed >= 62 && windSpeed <= 88) {// ----> good to specified used the and operatorr
        
        return "Tropical Storm Detected.";
      
        
    }else if (windSpeed >= 89 || windSpeed <=117) {
        return "Serve tropical Storm Detected";
    }
    
    else{
        return "Typhon Detected";
    }
    
}
let typhoonMessage1 = deterMIneTyphoonIntensity(29)
let typhoonMessage2 = deterMIneTyphoonIntensity(62)
let typhoonMessage3 = deterMIneTyphoonIntensity(61)
let typhoonMessage4 = deterMIneTyphoonIntensity(88)
let typhoonMessage5 = deterMIneTyphoonIntensity(117)
let typhoonMessage6 = deterMIneTyphoonIntensity(120)



console.log(typhoonMessage1);
console.log(typhoonMessage2);
console.log(typhoonMessage3);
console.log(typhoonMessage4);
console.log(typhoonMessage5);
console.log(typhoonMessage6);



// Truthy and falsy values 

// in jS, ther are values that are considerd "'Truthy", which means ia an booleal 



// sample of truthy
// 1.
// 2.
if(1){
    console.log("1 is truthy")
}

if([]){

    // Even thought the array is empty, it alReady exisxt, it in an existing
    console.log(" [] emppty array is truthy")
}

// falsy values are vulues considerd "false " in booleaan context like deterMIne an if condiotn


if (0) {
    console.log("0 is falsy.");
    
}

//  3
if (undefined) {
    console.log("undefined is not falsy");
    
}else{
    console.log("undefined is  falsy");
}



//codintionlay Termary operator

// Termary operator is used as shoter alertnative to if else statements 
// i


//syntax age = 17;
let age = 17;
let result = age < 18 ?  "underage " : " Legal"; /// ----> it  good for  short condition 
console.log(result);


// let result2  = if(age < 18) {

    
// }

// switch statements
// evaluate an exprerssion and match the expression to a case clause.
// An expression will be compared being evaluatees matched a case 
// It us uesd alternativlty from annif-else statements, howeve, if-else 
//statements provides more complexilty in its condition

let day = prompt("What day of the week iss it today?").toLowerCase();
console.log(day);

//switch statements to evaluatees the curent day and show a message to tell the user the color of the day
// if the the switch statements was not able to match a case eavaluated espressionm it will run the default case

// break0 keyword ends the case statements
switch (day) {
    case 'monday':
            console.log("The color of the day is red.");
        break;  ////////////////////////////////////-------> use the break for the purpose of next case will not execute
        case 'tuesday':
            console.log("The color of the day is orange.");
        break;
        case 'wensday':
            console.log("The color of the day is yellow.");
        break;
    default:
        console.log("Please enter a valid day");
        break;
}



// Mini acttivty



switch (day) {
    case 'monday':
            console.log("The color of the day is red.");
        break;  ////////////////////////////////////-------> use the break for the purpose of next case will not execute
        case 'tuesday':
            console.log("The color of the day is orange.");
        break;
        case 'wensday':
            console.log("The color of the day is yellow.");
        break;
        case 'thursday':
            console.log("The color of the day is block.");
        break;
        case 'friday':
            console.log("The color of the day is pink.");
        break;
        case 'sunday':
            console.log("The color of the day is blue");
        break;
    default:
        console.log("Please enter a valid day");
        break;
}


// try-catch-finally 

// we use to try-catch-finalay statementes to cach errors, disply and inform about the error an continue the code isntead of stoping
// we couyld alsod use the try-catch-finally statements to produce our own error message in the event of an error


// try allows us to run code, if ther is an eror in the intance of our code, then will be able to catch that eror in ou catch statements


try { ////=----> hadling error
    alert(deterMIneTyphoonIntensity(50))
} catch (error) {
    //wuith  the use of the typeof leywoprd. we will be able t retunr string which conaines information to what data type our error is string which containes information to what data type our eror us 

    // console.log(typeof error);

    console.log(error.message);
} finally{
    // finally statements will run regardless of the sucess or failure of the tey statements

    alert(" Intensity updates will show in a new alert");
}


console.log("will we continue to the next code?");